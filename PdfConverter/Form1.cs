﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PdfConverter
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonEdit1_Click(object sender, EventArgs e)
        {
            string filepath;
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                filepath = openFileDialog1.FileName;
                buttonEdit1.EditValue = filepath;

            }
            else
            {
                MessageBox.Show("No file selected");
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            var filepath = buttonEdit1.EditValue.ToString();
            if(filepath.Contains(' '))
            {
                MessageBox.Show("Please remove all white spaces from the name of the file and the file path");
                return;
            }
            var directory = Path.GetDirectoryName(filepath);

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.Arguments = string.Format("--zoom 1.5 --printing 0 --dest-dir {0}  {1}", directory, filepath);
            startInfo.FileName = Environment.CurrentDirectory + "\\pdf2htmlEX\\pdf2htmlEx.exe";
            //startInfo.UseShellExecute = false;
            //startInfo.RedirectStandardError = true;

            try
            {
                Process process = Process.Start(startInfo);
               /* string errors = process.StandardError.ReadToEnd();
                if(!string.IsNullOrEmpty(errors))
                {
                    MessageBox.Show(errors);
                }*/
                process.WaitForExit();

                if(checkEdit1.Checked)
                {
                    string start = string.IsNullOrEmpty(textEdit1.EditValue?.ToString()) ? "1" : textEdit1.EditValue?.ToString();
                    string end = string.IsNullOrEmpty(textEdit2.EditValue?.ToString()) ? "10" : textEdit2.EditValue?.ToString();
                    int startPage = int.Parse(start);
                    int endPage = int.Parse(end);
                    var previewDirectory = directory + "\\preview";

                    Directory.CreateDirectory(previewDirectory);
                    startInfo.Arguments = string.Format("-f {2} -l {3} --zoom 1.5 --printing 0 --dest-dir {1}  {0}", filepath, previewDirectory, startPage, endPage);

                    process = Process.Start(startInfo);
                    process.WaitForExit();

                    File.Move(Path.Combine(previewDirectory,Path.GetFileNameWithoutExtension(filepath) + ".html"), directory + "\\preview.html");
                    Directory.Delete(previewDirectory);
                }
               
                MessageBox.Show("Conversion finished. Check Source folder for output");

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
    }
}
